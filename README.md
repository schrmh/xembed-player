# xembed-player  
  
Ugh, another file you have to download to be able to see it?  
Fear not, xembed-player is your solution!  
It can embed itself into a app on your X11 desktop by making use of the XEmbed feature.  
No more leaving a outdated chat application to watch that cutting edge meme (#freeJXL)!
  
(Note: Currently you still have to use DISPLAY=:420 to launch something into xembed-player)  
  
Compile by using:  
`gcc -o xembed-player xembed-player.c -lX11`

Misc.  
I noticed that after embedding the first window into another one  
and the parent is then unmapped, even the second window gets unmapped.  
Originally I had this code in; you might want to add it if you really need it  
(e.g. in case you don't want to embed the app for some reason):  
```
         else if (event.type == UnmapNotify && event.xunmap.window == window) {
             // If the first window is unmapped, unmap the second window
             if (sub_window != None) {
                 XUnmapWindow(display, sub_window);
             }
         }
         else if (event.type == MapNotify && event.xmap.window == window) {
             // If the first window is remapped, remap the second window
             if (sub_window != None) {
                 XMapWindow(display, sub_window);
```
