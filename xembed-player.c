#include <stdio.h>
#include <X11/Xlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>

int main ( int argc, char *argv[] )
{
    Display *display = XOpenDisplay(NULL);
    Window parent_window;

    if ( argc != 2 )
    {
        printf("Call it with a X window ID that you can get e.g. by using xwininfo\n");
        if (argc > 2) return 0;
        parent_window = DefaultRootWindow(display);
    } else parent_window = strtol(argv[1], NULL, 0);

    Window window = XCreateSimpleWindow(display, parent_window, 0, 0, 32, 32, 0, 0, BlackPixel(display, DefaultScreen(display)));

    XSelectInput(display, window, ExposureMask | ButtonPressMask | StructureNotifyMask);
    XMapWindow(display, window);

    GC gc = XCreateGC(display, window, 0, NULL);

    XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));

    Window sub_window = None;
    bool sub_window_created = 0;
    XEvent event;
    while (1) {
        XNextEvent(display, &event);

        if (event.type == Expose) {
            XDrawString(display, window, gc, 10, 20, "~", 1);
        }
        else if (event.type == ButtonPress) {
            if (sub_window_created) {
                XWindowAttributes wa;
                if (XGetWindowAttributes(display, sub_window, &wa) && wa.map_state == IsViewable) {
                    XUnmapWindow(display, sub_window);
                }
                else {
                    XMapWindow(display, sub_window);
                }
            }
            else {
                XSetWindowAttributes sub_window_attr;
                sub_window_attr.colormap = XDefaultColormap(display, XDefaultScreen(display));
                sub_window_attr.override_redirect = True;
                sub_window = XCreateWindow(display, parent_window, 0, 0, 200, 200, 0, DefaultDepth(display, DefaultScreen(display)), InputOutput, DefaultVisual(display, DefaultScreen(display)), CWOverrideRedirect | CWColormap, &sub_window_attr);
                XMapWindow(display, sub_window);

                // Call Xephyr to create a X server which displays everything started with DISPLAY=:420 in the second window
                char command[100];
                sprintf(command, "Xephyr :420 -parent %lu &", sub_window);
                system(command);

                sub_window_created = 1;

                // Get the position of the first window and move the second window to that position (but in y direction + 300 pixels)
                int win_x, win_y;
                Window child;
                XTranslateCoordinates(display, window, parent_window, 0, 0, &win_x, &win_y, &child);
                XMoveWindow(display, sub_window, win_x, win_y + 300);
            }
        }
        else if (event.type == ConfigureNotify) {
            // If the first window is moved, move the second window to the new position to always keep it 300 pixel below
            if (sub_window != None) {
                int win_x, win_y;
                Window child;
                XTranslateCoordinates(display, window, parent_window, 0, 0, &win_x, &win_y, &child);
                XMoveWindow(display, sub_window, win_x, win_y + 300);
            }
        }
    }

    XCloseDisplay(display);

    return 0;
}
